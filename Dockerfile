FROM php:5.6
ENV DEBIAN_FRONTEND noninteractive

# install apache, mysql and php
RUN \
apt-get update -y && \
apt-get install -y apache2 && \
apt-get install -y mysql-server libapache2-mod-auth-mysql php5-mysql && \
apt-get install -y php5 libapache2-mod-php5 php5-mcrypt && \
apt-get clean